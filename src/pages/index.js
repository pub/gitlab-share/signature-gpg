import * as React from "react";
import { useState } from "react";
import PJson from "../../package.json";

import {
  Container,
  Header,
  HeaderBody,
  Logo,
  Service,
  SwitchTheme,
  Tool,
  ToolItem,
  ToolItemGroup,
  Row,
  Col,
} from "@dataesr/react-dsfr";

// markup
const IndexPage = () => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <Container>
      <Header>
        <HeaderBody>
          <Logo splitCharacter={10}>
            Ministères de la transition écologique et de la cohésion des
            territoires
          </Logo>
          <Service
            link="/dam/gitlab/gitlab-usage"
            title="Le produit dont vous êtes le héros !"
            description="Auto-évaluation pour vous aider à lancer votre produit"
          />
          <Tool closeButtonLabel="fermer">
            <ToolItemGroup>
              <ToolItem onClick={() => setIsOpen(true)}>
                <span
                  className="fr-fi-theme-fill fr-link--icon-left"
                  aria-controls="fr-theme-modal"
                  data-fr-opened={isOpen}
                >
                  Paramètres d’affichage
                </span>
              </ToolItem>
            </ToolItemGroup>
          </Tool>
        </HeaderBody>
      </Header>
      <SwitchTheme isOpen={isOpen} setIsOpen={setIsOpen} />
      <Row>
        <Col spacing="ml-auto">
          <h1>Bienvenue !</h1>
          <p>
            Bienvenue sur cet outil d'auto-évaluation. Version {PJson.version}
          </p>
        </Col>
      </Row>
    </Container>
  );
};

export default IndexPage;
