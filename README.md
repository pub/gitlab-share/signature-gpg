<p align="center">
  <img alt="Gitlab Logo" src="https://upload.wikimedia.org/wikipedia/commons/e/e1/GitLab_logo.svg" height="40"/> <span style="display: table-cell;
  vertical-align: middle;">+</span>
    <img alt="GPG Logo" src="https://upload.wikimedia.org/wikipedia/commons/6/61/Gnupg_logo.svg" height="40" />
</p>
<h1 align="center">
  Signature de code avec GPG et Gitlab
</h1>

## 💾 Installation d'un client GPG

1.  **Sous Linux**

    Utilisation du package manager de votre distribution. Avec Debian/Ubuntu :

    ```sh
    sudo apt update
    sudo apt install gnupg gpg-agent
    ```

2.  **Sous Windows**

    Installation possible avec l'outil [gpg4win](https://www.gpg4win.org/). Avec [Chocolatey](https://community.chocolatey.org/), l'installation est possible en ligne de commande (Powershell mode admin) :

    ```sh
    choco install gpg4win
    ```

3.  **Sous MacOS**

    Avec [Brew](https://brew.sh/index_fr)

    ```sh
    brew install gnupg
    ```

## 🔑 Création d'une paire de clés et export

1.  Créer une paire de clés

    ```sh
    gpg --full-gen-key
    # répondre aux question (prendre les valeurs par défaut)
    # saisir votre Nom Prénom, mail complet et en commentaire éventuellement le nom DNS ou l'ip de la machine où se trouve votre clef privé: le serveur courant)
    # Nom réel : Elliot Alderson
    # Adresse électronique : elliot.alderson@developpement-durable.gouv.fr
    # Commentaire : Laissez libre cours à votre imagination, ou laissez vide ;)
    # Saisir ensuite votre phrase secrète pour votre clef GnuPG
    ```

    **Veillez à renseigner la même adresse e-mail que celle utilisée dans Gitlab !**

2.  **Obtenir l'ID de votre clé**

    Indiquez la même adresse e-mail que celle issue de la génération de clés.

    ```sh
    gpg --list-secret-keys --keyid-format LONG <email>
    ```

    Vous obtiendrez la sortie suivante :

    ```sh
    sec   rsa3072/2DE37ABCF8EE2A03 2022-04-26 [SC]
          46B62F537BBA2516735420912DE37ABCF8EE2A03
    uid                [  ultime ] Elliot Alderson <elliot.alderson@developpement-durable.gouv.fr>
    ssb   rsa3072/E3A753935381E7C4 2022-04-26 [E]
    ```

    L'ID de clé est l'identifiant à côté de rsa3072 (ou autre nombre selon la longueur choisie), par exemple ici, il s'agit de : `2DE37ABCF8EE2A03`.

3.  **Exporter la clé publique**

    Indiquez l'ID obtenu par la commande précédente dans la commande :

    ```sh
    gpg --armor --export <id>
    ```

    Vous obtiendrez la sortie suivante :

    ```sh
    -----BEGIN PGP PUBLIC KEY BLOCK-----
    ...
    ...
    ...
    -----END PGP PUBLIC KEY BLOCK-----
    ```

    Il s'agit de votre clé GPG publique, celle que vous allez renseigner dans Gitlab.

## 🦊 Ajout de la clé publique dans Gitlab

1. Sur l'instance Gitlab, se rendre dans les [préférences utilisateurs](https://gitlab-forge.din.developpement-durable.gouv.fr/-/profile/preferences)
2. Puis se rendre sur l'item [GPG Keys](https://gitlab-forge.din.developpement-durable.gouv.fr/-/profile/gpg_keys).
3. Copier/coller le bloc de la clé publique, avec les blocs `-----BEGIN PGP PUBLIC...-----` et `-----END PGP PUBLIC...-----` et valider
4. Votre clé est ajoutée et indiquée comme vérifiée. Il est possible d'en ajouter d'autres et de les révoquer.

## 💻 Utiliser la clé GPG avec Git

1. **Vérifier la configuration de l'auteur de commit**

   > ⚠️ **Attention !** Veillez à bien utiliser l'email de signature GPG dans vos commits Git. Pour rappel, voici les commandes pour configurer l'auteur et le mail des commits :

   ```sh
   git config user.name "<Prénom Nom>"
   git config user.email "<email>"
   ```

   > ℹ️ Toutes le commandes git config sont valables par défaut au niveau de votre dépôt courant uniquement. Si vous souhaitez appliquer globalement une configuration au niveau de votre compte utilisateur, utilisez la commande `git config --global` au lieu de `git config`.

2. **Utiliser la clé GPG à chaque commit**

   Dans votre dépôt courant, utilisez les commandes :

   ```sh
   git config user.signingkey <ID de clé>
   git config commit.gpgsign true
   ```

   L'ID de clé est le même que celui utiliser pour obtenir l'export, `2DE37ABCF8EE2A03` dans notre exemple.

   La première commande indique quelle clé utiliser pour signer. La deuxième commande systématise l'utilisation de la signature pour chaque commit. C'est utile pour éviter d'ajouter l'option `-S` à `git commit` et pour effectuer un commit depuis un IDE, comme VSCode.

   Maintenant, chacun de vos commits sera signé ! Pour activer cette configuration globalement, vous pouvez utiliser l'option `--global`.

3. **Signer aussi les tags**

   Un tag aussi peut être signé, à condition que ce soit un tag de type [annotated](https://git-scm.com/book/en/v2/Git-Basics-Tagging). Pour créer un tag annoté et signé, il suffit de lancer la commande :

   ```sh
   git tag -s <montag>
   ```

## 🔍 Vérifier la signature

1. **Avec Git**

   La commande suivante permet de vérifier un commit :

   ```sh
   git verify-commit <référence commit>
   ```

   La référence d'un commit peut être une branche, un hash de commit, ou même HEAD pour le commit courant sur votre système de fichiers. Si le commit est signé et que la clé utilisée est dans votre trousseau GPG, vous aurez le retour suivant :

   ```sh
   gpg: Signature faite le mar. 26 avril 2022 11:27:52 CEST
   gpg:                avec la clef RSA 46B62F537BBA2516735420912DE37ABCF8EE2A03
   gpg: Bonne signature de « Elliot Alderson <elliot.alderson@developpement-durable.gouv.fr> » [ultime]
   ```

   Si le commit n'est pas signé, la commande renvoie un code d'erreur, sans message. Il est très important de noter que l'autorité de certification est votre machine locale ici. Si vous n'avez pas ajouté la clé de votre collègue sur votre trousseau GPG, la signature ne sera pas "vérifiée" par cette commande.

   > ℹ️ [Pour en savoir plus sur les commandes GPG](https://linuxfr.org/users/gouttegd/journaux/bien-demarrer-avec-gnupg).

   Il est possible de vérifier aussi un tag avec la commande :

   ```sh
   git verify-tag <montag>
   ```

   Si vous avez le message d'erreur :

   ```sh
   error: <montag>: cannot verify a non-tag object of type commit.
   ```

   Cela vient du fait que votre tag n'est pas un tag _annotated_ mais un tag simple, dit _lightweight_.

2. **Avec l'interface web Gitlab**

   Dans Gitlab, une fois les commits pushés, il est possible de voir s'ils sont signés et de vérifier leur signature en utilisant Gilab comme autorité de certification.

   Pour cela, sur votre dépôt il suffit d'aller dans le menu Repository -> Commits. La liste des commits s'affiche par branche, et un badge "Verified" s'affiche pour chaque commit signé et vérifié.

   À ce jour, il n'est pas possible de vérifier via l'interface Gitlab la signature d'un tag.

3. **Avec l'API Gitlab**

   L'API Gitlab permet aussi de vérifier une signature. Ceci permet potentiellement de l'utiliser dans un pipeline CI. Voici un exemple d'usage, nécessitant le hash du commit.

   La variable `$TOKEN` contient votre token d'application. Dans un job de pipeline CI, il faudrait remplacer `PRIVATE-TOKEN` par `JOB-TOKEN`. Un exemple à venir devrait être pubié ici.

   ```sh
   curl --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/<id du projet>/repository/commits/<hash du commit>/signature" | jq .
   ```

   Exemple de réponse si le commit est signé et vérifié :

   ```json
   {
     "signature_type": "PGP",
     "verification_status": "verified",
     "gpg_key_id": 5,
     "gpg_key_primary_keyid": "2DE37ABCF8EE2A03",
     "gpg_key_user_name": "Elliot Alderson",
     "gpg_key_user_email": "elliot@devcooc.fr",
     "gpg_key_subkey_id": null,
     "commit_source": "gitaly"
   }
   ```

4. **Avec VSCode**

   VSCode peut fournir une visualisation de signature par l'intermédiaire de plugin.

   Par exemple, avec le plugin populaire [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph) :

   1. Activer la visualisation de signature en allant dans les settings et activer l'option `Git-graph › Repository › Commits: Show Signature Status`
   2. Aller dans la visualisation de l'historique Git : icône de branches à gauche dans le menu latéral, puis icône Git Graph indiquant `View Git Graph`.
   3. En cliquant sur un commit, un symbole de validation ✅ apparait à côté de l'auteur du commit, si le commit est signé et vérifié par Git.
