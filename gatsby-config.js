module.exports = {
  siteMetadata: {
    siteUrl:
      "https://snum.gitlab-pages.din.developpement-durable.gouv.fr/dam/gitlab/signature-gpg/",
    title: "Signature GPG démo",
  },
  pathPrefix: "/dam/gitlab/signature-gpg",
  plugins: [],
};
